FROM registry.access.redhat.com/ubi9/ubi:9.3-1610
WORKDIR /opt/
RUN yum install unzip java-11-openjdk-devel  which -y && \
curl https://public.dhe.ibm.com/ibmdl/export/pub/software/websphere/wasdev/downloads/wlp/24.0.0.7/wlp-jakartaee10-24.0.0.7.zip -o wlp.zip && \
curl https://gitlab.com/reddi.devops/wasliberty/-/raw/main/server.xml -o server.xml && \
curl https://gitlab.com/reddi.devops/wasliberty/-/raw/main/session.war -o session.war && \
curl  https://gitlab.com/reddi.devops/wasliberty/-/raw/main/JavaHelloWorldApp.war -o JavaHelloWorldApp.war && \
unzip wlp.zip && \
rm -rf wlp.zip && \
ln -sf "/usr/share/zoneinfo/Asia/Kolkata" /etc/localtime && \
/opt/wlp/bin/server create server1  && \
rm -rf /opt/wlp/usr/servers/server1/server.xml
COPY server.xml /opt/wlp/usr/servers/server1/
COPY session.war /opt/wlp/usr/servers/server1/dropins/session.war
COPY JavaHelloWorldApp.war /opt/wlp/usr/servers/server1/dropins/
EXPOSE 9080
EXPOSE 9443
ENTRYPOINT ["/opt/wlp/bin/server", "run", "server1"]
